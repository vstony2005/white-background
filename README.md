# Background white

## Description

Project created with Delphi 10.
Allows you to have a single color background. I use it to take screenshots.

## Icons

Images used from [WPZOOM Developer icon pack](https://findicons.com/pack/2448/wpzoom_developer)

## License

License MIT
