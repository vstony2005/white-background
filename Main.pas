unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.ImageList, Vcl.ImgList,
  Vcl.VirtualImageList, Vcl.BaseImageCollection, Vcl.ImageCollection, Vcl.Menus;

type
  TFrmMain = class(TForm)
    PopupMenu1: TPopupMenu;
    ColorDialog1: TColorDialog;
    ImageCollection1: TImageCollection;
    VirtualImageList1: TVirtualImageList;
    mnColor: TMenuItem;
    N1: TMenuItem;
    mnExpand: TMenuItem;
    mnCollapse: TMenuItem;
    N2: TMenuItem;
    mnExit: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
    procedure mnCollapseClick(Sender: TObject);
    procedure mnColorClick(Sender: TObject);
    procedure mnExitClick(Sender: TObject);
    procedure mnExpandClick(Sender: TObject);
  private
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

procedure TFrmMain.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style := Params.Style or WS_BORDER or WS_THICKFRAME;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  Self.BorderStyle := bsNone;
  Self.Position := poScreenCenter;
  Self.Color := clWhite;
end;

procedure TFrmMain.FormDblClick(Sender: TObject);
begin
  if (WindowState = wsMaximized) then
    WindowState := wsNormal
  else
    WindowState := wsMaximized;
end;

procedure TFrmMain.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
begin
  if (Button <> mbRight) then
  begin
    ReleaseCapture;
    SendMessage(FrmMain.Handle, WM_SYSCOMMAND, $F012, 0);
  end;
end;

procedure TFrmMain.mnCollapseClick(Sender: TObject);
begin
  WindowState := wsNormal
end;

procedure TFrmMain.mnColorClick(Sender: TObject);
begin
  ColorDialog1.Color := Self.Color;
  if (ColorDialog1.Execute) then
    Self.Color := ColorDialog1.Color;
end;

procedure TFrmMain.mnExitClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFrmMain.mnExpandClick(Sender: TObject);
begin
  WindowState := wsMaximized;
end;

end.
